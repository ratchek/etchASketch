$(document).ready(function(){

initializeGrid(40);


$("#clearGridButton").click( function(){ 
	var size = prompt("How many squares per row? ");
	size = parseInt(size);
	console.log(size);
	initializeGrid(size) });


});


function initializeGrid(sideSize){
$('#wrapper div').empty();
var nodeDimensions = determineNodeDimensions(sideSize);
var rowDimensions = nodeDimensions+4; //rows need to take into account margins of nodes.
nodeDimensions = nodeDimensions.toString() + "px";
rowDimensions = rowDimensions.toString() + "px";
console.log(nodeDimensions);


for(var i=0; i<sideSize; i++){
 	var $newRowDiv="<div id='row_"+i+"' class = 'row' </div> ";
 	$("#wrapper").append($newRowDiv);
 	for(var j = 0; j<sideSize; j++){  //10 columns
		var $newNodeDiv="<div id='"+"1"+"x"+j+"' class= 'node' </div> ";
		$("#row_"+i).append($newNodeDiv);
	}
 }

$(".node").css("height", nodeDimensions);
$(".node").css("width", nodeDimensions);
$(".row").css("height", rowDimensions);


$(".node").mouseenter(function(){
	$(this).css("background-color","red");
});

};

// determines how big each node must be so that the given number fits in the alloted space.
// assumes margin will be 2px and border thickness will be 0px
function determineNodeDimensions(sideSize){
	console.log("hello");

	var wrapperDimensions = $("#wrapper").css("height"); 
	// convert the string to number, dropping the px
	wrapperDimensions = parseInt(wrapperDimensions);
	var nodeDimensions = wrapperDimensions/sideSize;
	var nodeDimensions = nodeDimensions-4;
	return nodeDimensions;
};
